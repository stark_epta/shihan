<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены что хотите удалить новость?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Название',
                'attribute' => 'name'
            ],
            [
                'label' => 'Описание',
                'attribute' => 'description',
                'format' => 'html'
            ],

            'meta_title',
            'meta_description',
            'meta_keywords',
            [
                'label' => 'Время создания',
                'attribute' => 'created_at',
                'format' => 'date'
            ],
            [
                'label' => 'Время редактирования',
                'attribute' => 'updated_at',
                'format' => 'date'
            ],

        ],
    ]) ?>

</div>
