<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pages', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'id' => 'sortable',
            'class' => 'table table-striped table-bordered'
        ],
        'rowOptions' => function ($model, $key, $index, $grid) {
            return ['id' => 'items[]_' . $model['id']];
        },
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'url:url',
            [
                'attribute' => 'menu',
                'content' => function ($data) {
                    if ($data == 1) {
                        return 'Да';
                    } else {
                        return 'Нет';
                    }
                }
            ],
            [
                'attribute' => 'status',
                'content' => function ($data) {
                    if ($data == 1) {
                        return 'Включена';
                    } else {
                        return 'Выключена';
                    }
                }
            ],
            [
                'attribute' => 'dropdown_menu',
                'label' => 'Выпадающее меню',

            ],
            // 'meta_title',
            // 'meta_description',
            // 'meta_keywords',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<div class="hidden table_name">Pages</div>
