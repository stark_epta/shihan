<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название') ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Описание')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['/admin/common/image-upload']),
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]); ?>
    <?
    echo \yii\helpers\Html::label('Images');

    echo \kartik\file\FileInput::widget([
        'name' => 'image',
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'uploadUrl' => \yii\helpers\Url::to(['file-upload']),
            'removeUrl' => 'dsadas',
            'overwriteInitial' => false,
            'allowedFileExtensions' =>  ['jpg', 'png','gif'],
            'initialPreview'=>$image,
            'showUpload' => true,
            'showRemove' => false,
            'dropZoneEnabled' => false
        ]
    ]);
    ?>
    <?
    echo \yii\helpers\Html::label('Images');

    echo \kartik\file\FileInput::widget([
        'name' => 'image',
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'uploadUrl' => \yii\helpers\Url::to(['file-upload']),
            'removeUrl' => 'dsadas',
            'overwriteInitial' => false,
            'allowedFileExtensions' =>  ['jpg', 'png','gif'],
            'initialPreview'=>$image,
            'showUpload' => true,
            'showRemove' => false,
            'dropZoneEnabled' => false
        ]
    ]);
    ?>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Инструменты для сео
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true])->label('Meta Title (Для сео)') ?>

                    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true])->label('Meta Description (Для сео)') ?>

                    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true])->label('Meta Description (Для сео)') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function deletefile(id) {

        $.post('delete-file?id='+id,{},function (data) {
              if (data) {
                  $('.file-preview-initial').remove();
                  alert('Изображение удалено!');
              }
        })
    }
</script>

