<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget;

?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название продукта') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Описание')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['/admin/common/image-upload']),
            'plugins' => [
                'clips',
                'fullscreen'
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'category_id')->dropDownList($category_select)->label('Выберите категорию'); ?>

    <?php if ($images_prev): ?>
        <div class="preview-image">
            <?php foreach ($images_prev as $image): ?>
                <div class="img-prev-caption">
                    <img src="/uploads/products/<?= $model->id ?>/<?= $image; ?>">
                    <a href="#" class="btn btn-danger deleteImg" data-id="<?= $model->id ?>" data-file="<?= $image; ?>">Удалить</a>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="clearfix"></div>
    <?php endif; ?>

    <?= $form->field($model, 'imageFile[]')->fileInput(['multiple' => true, 'accept' => 'image/*'])->label('Паспорта продукции'); ?>

    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        Инструменты для сео
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true])->label('Meta Title (Для сео)') ?>

                    <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true])->label('Meta Description (Для сео)') ?>

                    <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true])->label('Meta keywords (Для сео)') ?>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Рекдактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $('.deleteImg').on('click', function () {
        var file = $(this).attr('data-file');
        var id = $(this).attr('data-id');
        $.post('/admin/product/delete-file?file=' + file, {'id': id}, function (data) {
            alert('Изображение удалено!');
        });
        $(this).parent().remove();
        return false;
    })
</script>
