<?php
namespace app\modules\admin\controllers;

use app\models\Category;
use yii\web\Controller;

class CommonController extends Controller
{

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => 'http://shihan-oil.com/uploads/editor', // Directory URL address, where files are stored.
                'path' => '@app/public_html/uploads/editor' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionSort($table = false) {
        if($table) {
            if(isset($_POST['items']) && is_array($_POST['items'])) {
                foreach ($_POST['items'] as $order => $id) {
                    $db = \Yii::$app->db;
                    $db->createCommand('UPDATE '.$table.' SET ordering = :order WHERE id = :id')
                        //->bindValue(':table', $table)
                        ->bindValue(':order', $order)
                        ->bindValue(':id', $id)
                        ->execute();

                }
            }
        }
    }
}