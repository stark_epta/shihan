<?php

namespace app\modules\admin\controllers;

use Codeception\PHPUnit\Constraint\Page;
use Yii;
use app\models\Pages;
use app\models\PagesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('ordering');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();
        $pages_select = ArrayHelper::map(Pages::find()->where(['dropdown_menu' => 0])->andWhere(['<>','type','catalog'])->all(), 'id', 'name');
        $max_ordering = Pages::find()->max('ordering');
        $modelHasDropdown = false;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $dropdown_menu = $model->dropdown_menu;
            if ($dropdown_menu == 0) {
                $model->parent_id = null;
            } elseif ($dropdown_menu == 1) {
                $modelHasDropdown = Pages::find()->where(['id' => $model->parent_id])->one();
                $modelHasDropdown->has_dropdown = 1;
            }
            if ($model->save()) {
                if ($modelHasDropdown) {
                    $modelHasDropdown->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'pages_select' => $pages_select
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $pages_select = ArrayHelper::map(Pages::find()->where(['<>', 'id', $id])->andWhere(['<>','type','catalog'])->andWhere(['dropdown_menu' => 0])->all(), 'id', 'name');
        $modelHasDropdown = false;
        $deleteOldHasDropdown = false;
        $parent_id = $model->parent_id;
        if ($model->load(Yii::$app->request->post())) {
            $dropdown_menu = $model->dropdown_menu;
            $modelHasDropdown = Pages::find()->where(['id' => $parent_id])->one();
            if ($dropdown_menu == 0) {
                $modelHasParentId = Pages::find()->where(['parent_id' => $parent_id])->all();
                $arrayHasParentId = [];
                foreach ($modelHasParentId as $item) {
                    $arrayHasParentId[] = $item->parent_id;
                }
                if (count($arrayHasParentId) <= 1) {
                    if ($modelHasDropdown->has_dropdown == 1) {
                        $modelHasDropdown->has_dropdown = NULL;
                    }
                }
                $model->parent_id = null;
            } elseif ($dropdown_menu == 1) {
                $currentParent = Yii::$app->request->post()['Pages']['parent_id'];
                if ($parent_id !== null) {
                    if ($currentParent != $parent_id) {
                        $modelHasParentId = Pages::find()->where(['parent_id' => $parent_id])->all();
                        $arrayHasParentId = [];
                        foreach ($modelHasParentId as $item) {
                            $arrayHasParentId[] = $item->parent_id;
                        }
                        if (count($arrayHasParentId) <= 1) {
                            $deleteOldHasDropdown = Pages::find()->where(['id' => $parent_id])->one();
                            $deleteOldHasDropdown->has_dropdown = NULL;
                        }
                    }
                }
                $modelHasDropdown = Pages::find()->where(['id' => $currentParent])->one();
                $modelHasDropdown->has_dropdown = 1;
            }
            if ($model->save()) {
                if ($deleteOldHasDropdown) {
                    $deleteOldHasDropdown->save();
                }
                if ($modelHasDropdown) {
                    $modelHasDropdown->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'pages_select' => $pages_select
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $parent_id = Pages::findOne($id)->parent_id;
        $parent_page = Pages::findOne($parent_id);
        if ($this->findModel($id)->delete()) {
            if ($parent_page) {
                $parent_page->has_dropdown = NULL;
                $parent_page->save();
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
