<?php

namespace app\modules\admin\controllers;

use app\models\Product;
use Yii;
use app\models\Category;
use app\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy('ordering');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        $image = '';
        $imageAdd = Yii::$app->cache->get('image');
        $max_ordering = Category::find()->max('ordering');
        if ($model->load(Yii::$app->request->post())) {
            $model->ordering = $max_ordering+1;
            if ($imageAdd) {
                $model->image = $imageAdd;
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'image' => $image
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $image = '';
        $imageAdd = Yii::$app->cache->get('image');
        $model = $this->findModel($id);
        if (isset($model->image)) {
            $image = '<img src = "/uploads/category/'.$model->image.'" width="250"><br><br><a onclick = \'deletefile('.$id.')\' class="btn btn-danger" id="xyu">Удалить</a>';
        }
        if ($imageAdd) {
            $model->image = $imageAdd;
        }
      //  href="delete-file?id='.$id.'"
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'image' => $image
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id)->delete()) {
            Product::deleteAll(['category_id' => $id]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionFileUpload(){
        $model = new Category();
        if(Yii::$app->request->isPost){
            $id = $model->id;
            $path = Yii::getAlias("@app/web/uploads/category");
            $file = UploadedFile::getInstanceByName('image');
            $name = time().'.'.$file->extension;
            $file->saveAs($path .DIRECTORY_SEPARATOR .$name);
            $image  = $path .DIRECTORY_SEPARATOR .$name;
            Yii::$app->cache->set('image',$name);
            return true;
        }
    }
    public function actionDeleteFile($id) {

        $model = $this->findModel($id);
        $image = $model->image;
        $path = Yii::getAlias("@app/web/uploads/category");
        $file = $path.'/'.$image;
        if (is_file($file)) {
            if (unlink($file)) {
                $model->image = NULL;
                $model->save();
                return true;
            }
        }

    }
}
