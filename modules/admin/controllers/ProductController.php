<?php

namespace app\modules\admin\controllers;

use app\models\UploadFile;
use Yii;
use app\models\Product;
use app\models\ProductSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Category;
use yii\helpers\BaseFileHelper;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use Imagine\Image\Point;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {


        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Product();
        $category_select = \yii\helpers\ArrayHelper::map(Category::find()->all(), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $path = Yii::getAlias("@app/public_html/uploads/products/" . $model->id);
            BaseFileHelper::createDirectory($path);
            $files = UploadedFile::getInstances($model, 'imageFile');

            foreach ($files as $file) {
                $image = $path . DIRECTORY_SEPARATOR . time() . '.' . $file->extension;
                $file->saveAs($image);
                $size = getimagesize($image);
                $width = $size[0];
                $height = $size[1];
                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . time() . '.' . $file->extension;
                Image::frame($image, 0, '666', 0)
                    ->crop(new Point(0, 0), new Box($width, $height))
                    ->resize(new Box(200, 300))
                    ->save($new_name, ['quality' => 100]);
                sleep(1);
            }
            //$file->saveAs($path . DIRECTORY_SEPARATOR . $fileName);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'category_select' => $category_select
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $category_select = \yii\helpers\ArrayHelper::map(Category::find()->all(), 'id', 'name');

        $imagesPath = Yii::getAlias("@app/public_html/uploads/products/" . $id);

        if (is_dir($imagesPath)) {
            $filesPrev = FileHelper::findFiles($imagesPath);

            $images_prev = [];

            foreach ($filesPrev as $filePrev) {
                if (strstr($filePrev, "small_")) {
                    $images_prev[] = basename($filePrev);
                }
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $path = Yii::getAlias("@app/public_html/uploads/products/" . $model->id);
            BaseFileHelper::createDirectory($path);
            $files = UploadedFile::getInstances($model, 'imageFile');

            foreach ($files as $file) {
                $image = $path . DIRECTORY_SEPARATOR . time() . '.' . $file->extension;
                $file->saveAs($image);
                $size = getimagesize($image);
                $width = $size[0];
                $height = $size[1];
                $new_name = $path . DIRECTORY_SEPARATOR . "small_" . time() . '.' . $file->extension;
                Image::frame($image, 0, '666', 0)
                    ->crop(new Point(0, 0), new Box($width, $height))
                    ->resize(new Box(200, 300))
                    ->save($new_name, ['quality' => 100]);
                sleep(1);
            }
            //$file->saveAs($path . DIRECTORY_SEPARATOR . $fileName);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'images_prev' => $images_prev,
                'category_select' => $category_select
            ]);
        }
    }
    public function actionDeleteFile($file) {
        $or_file = trim($file,'small_');
        $id = \Yii::$app->request->post('id');
        $path = \Yii::getAlias("@app/public_html/uploads/products/".$id."/");
        if (unlink($path.$file) && unlink($path.$or_file)) {
            return true;
        }
        return false;

    }
    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $path = \Yii::getAlias("@app/public_html/uploads/products/".$id);
        if ($this->findModel($id)->delete()) {
            if (file_exists($path)) {
                foreach (glob($path.'/*') as $file) {
                    unlink($file);
                }
                rmdir($path);

            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
