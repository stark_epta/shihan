<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\models\Category;

class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    public static function tableName()
    {
        return 'product';
    }

    public $imageFile;
    public function rules()
    {
        return [
            [['description','meta_title','meta_description','meta_keywords'], 'string'],
            [['category_id'], 'integer'],
            [['name'], 'required'],
            [['description'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif','maxFiles' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'category_id' => 'Category ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getCategory() {
        return $this->hasOne(Category::className(),['id'=>'category_id']);
    }
}
