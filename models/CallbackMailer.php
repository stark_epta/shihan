<?php

namespace app\models;

use app\components\Common;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CallbackMailer extends Model
{
    public $name;
    public $email;
    public $phone;
    public $phonesend;
    public $subject;
    public $mailbody;


    public function rules()
    {
        return [
            [['subject',], 'required'],
            [['name'], 'required', 'message' => 'Необходимо заполнить поле "Имя"'],
            [['phone'], 'required', 'message' => 'Необходимо заполнить поле "Телефон"'],
            [['phonesend'], 'required', 'message' => 'Необходимо заполнить поле "Телефон"'],
            [['email'], 'required', 'message' => 'Необходимо заполнить поле "Email"'],
            [['email'], 'email', 'message' => 'Необходимо заполнить поле корректно "Email"'],
        ];
    }


    public function sendEmail($name, $phone = false, $subject, $email = false, $phonesend = false)
    {
        return Yii::$app->mailer->compose('view', [
            'name' => $name,
            'phone' => $phone,
            'subject' => $subject,
            'email' => $email,
            'phonesend' => $phonesend
        ])
            ->setFrom('shihanoil1@yandex.ru')
            ->setTo('shihanoil1@yandex.ru')
            ->setSubject($subject)
            ->send();
    }

    public function sendPrice($subject, $email)
    {
        $sendmail = Yii::$app->mailer->compose('price')
            ->setFrom('shihanoil1@yandex.ru')
            ->setTo($email)
            ->setSubject($subject)
            ->attach(Yii::getAlias('@app/web/images/banner.png'));

        return $sendmail->send();

    }
}
