<?php

    namespace app\models;

    use yii\base\Model;

    class SearchForm extends Model {
        public $search_query;



        /**
         * @return array the validation rules.
         */
        public function rules()
        {
            return [
                // name, email, subject and body are required
                [['search_query'], 'required','message' => 'Заполните поле'],
                ['search_query', 'string','min' => '3','tooShort' => 'Не менее 3-х символов'],
            ];
        }
    }