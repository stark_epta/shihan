<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SendPrice extends Model
{
    public $name;
    public $phone;
    public $subject;
    public $email;


    public function rules()
    {
        return [
            [['subject'], 'required'],
            [['name'], 'required','message' => 'Необходимо заполнить поле "Имя"'],
            [['phone'], 'required','message' => 'Необходимо заполнить поле "Телефон"'],
            [['email'], 'required','message' => 'Необходимо заполнить поле "Email"'],
            [['email'], 'email','message' => 'Необходимо заполнить поле "Email" корректно!'],
        ];
    }


    public function sendEmail($name,$phone,$subject)
    {
        return Yii::$app->mailer->compose('view',[
            'name' => $name,
            'phone' => $phone,
            'subject' => $subject
        ])
            ->setFrom('starkrenat@yandex.ru')
            ->setTo('artes_x@mail.ru')
            ->setSubject($subject)
            ->send();
    }
}
