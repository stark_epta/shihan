<?php

namespace app\models;

use Yii;
use app\models\Product;
use yii\behaviors\TimestampBehavior;

class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */


    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'],'safe'],
            [['name'], 'string', 'max' => 255],
            [['description','meta_title','meta_description','meta_keywords'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }



    public function getProducts() {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

}
