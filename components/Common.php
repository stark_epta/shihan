<?php

namespace app\components;

use app\models\Category;
use app\models\News;
use yii\helpers\FileHelper;
use yii\base\Component;

class Common extends Component {
    public static function getLeftCategories() {
        $model = Category::find()->orderBy('ordering')->all();
        return $model;
    }
    public static function getLeftNews() {
        $model = News::find()->orderBy('created_at')->limit(4)->all();
        return $model;
    }
    public static function getFormatDate($date) {
        return \Yii::$app->formatter->asDatetime($date, "php:d.m.Y ");
    }
    public static function getImagesFromPath() {
        $path = \Yii::getAlias('@app/web/files');
        $files = FileHelper::findFiles($path);
        $images = [];
        foreach ($files as $file) {
            $images[] = $file;
        }
        return $images;
    }
}