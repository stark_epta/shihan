<?php

use yii\db\Migration;

class m170207_142113_create_pages extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `pages` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `html` text,
  `menu` int(11) DEFAULT NULL,
  `dropdown_menu` int(11) NOT NULL,
  `parent_id` int(4) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `has_dropdown` int(4) DEFAULT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'page',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pages` (`id`, `name`, `url`, `html`, `menu`, `dropdown_menu`, `parent_id`, `status`, `has_dropdown`, `type`, `meta_title`, `meta_description`, `meta_keywords`, `ordering`, `created_at`, `updated_at`) VALUES
(2, 'О компании', 'about', '<p>вфывфы</p>', 1, 0, NULL, 1, NULL, 'page', 'dsa', '', '', 2, 1486479983, 1486581229),
(3, 'Контакты', 'contact', '<p>вфы</p>', 1, 0, NULL, 1, NULL, 'page', '', '', '', 0, 1486480016, 1486882318),
(5, 'Каталог продукции', 'catalog', '<p> s</p>', 1, 0, NULL, 1, 1, 'catalog', 'dasdas', '', '', 1, 1486489331, 1486622964);
        ");
    }

    public function down()
    {
        echo "m170207_142113_create_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
