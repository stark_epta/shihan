<?php

use yii\db\Migration;

class m170203_103426_create_category extends Migration
{
    public function up()
    {
        $this->execute("
        CREATE TABLE `category` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `ordering` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   INSERT INTO `category` (`id`, `name`, `description`, `ordering`, `image`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`) VALUES
(7, 'Присадки для бензина', '', 5, '1486892611.png', '', '', '', 1486128893, 1486892612),
(8, 'Светлые нефтепродукты', '', 1, '1486892567.png', '', '', '', 1486128904, 1486892568),
(9, 'Темные нефтепродукты', '', 6, '1486892629.png', '', '', '', 1486128965, 1486892631),
(10, 'Масла', '<p><img src=\"http://shihan.loc/uploads/editor/5898874421b8a.png\"></p><p>dasdasd</p>', 0, '1486890926.png', 'масла', 'масла дескр', 'масла кей', 1486129036, 1486890929),
(11, 'Газы', '', 4, '1486892594.png', '', '', '', 1486129125, 1486892596),
(12, 'Нефтехимия', '', 3, '1486892579.png', '', '', '', 1486134037, 1486892580);
        ");
    }

    public function down()
    {
        echo "m170203_103426_create_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
