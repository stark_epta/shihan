<?php
    namespace app\widgets;

    use app\models\SearchForm;
    use yii\bootstrap\Widget;


    class SearchWidget extends Widget {
        public function run() {
            $model = new SearchForm();
            return $this->render('search',['model' => $model]);
        }
    }