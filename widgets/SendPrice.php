<?php
namespace app\widgets;

use app\models\SearchForm;
use yii\bootstrap\Widget;


class SendPrice extends Widget {
    public function run() {

        $model = new \app\models\CallbackMailer();
        return $this->render('send-price',['model' => $model]);
    }
}