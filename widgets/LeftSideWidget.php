<?php
namespace app\widgets;

use app\models\SearchForm;
use yii\bootstrap\Widget;


class LeftSideWidget extends Widget {
    public function run() {
        return $this->render('leftside');
    }
}