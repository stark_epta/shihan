<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php
                $model_menu = \app\models\Pages::find()
                    ->where(['status' => 1])
                    ->andWhere(['menu' => 1])
                    ->andWhere(['dropdown_menu' => 0])
                    ->orderBy('ordering')
                    ->all();
                $model_category = \app\models\Category::find()->orderBy('ordering')->all();

                foreach ($model_menu as $item):
                    $model_dropdown = \app\models\Pages::find()
                        ->where(['status' => 1])
                        ->andWhere(['menu' => 1])
                        ->andWhere(['dropdown_menu' => 1])
                        ->andWhere(['parent_id' => $item->id])
                        ->all();

                    ?>
                    <?php if ($item->has_dropdown == 1): ?>
                    <li class="dropdown">
                    <a class="dropdown-toggle" href="/<?= $item->url ?>">
                        <?= $item->name ?>
                    </a>
                <?php else: ?>
                    <li>
                    <a href="/<?= $item->url ?>">
                        <?= $item->name ?>
                    </a>
                <?php endif; ?>
                    <?php if ($item->type == 'page'): ?>
                    <?php if ($item->has_dropdown == 1): ?>
                        <ul class="dropdown-menu">
                            <?php foreach ($model_dropdown as $dropdown): ?>
                                <li><a href="/<?= $dropdown->url ?>"><?= $dropdown->name ?> </a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
                    <?php if ($item->type == 'catalog'): ?>
                    <ul class="dropdown-menu">
                        <?php foreach ($model_category as $category): ?>
                            <li><a href="/catalog/<?= $category->id ?>"><?= $category->name ?> </a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                    </li>

                <?php endforeach; ?>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>