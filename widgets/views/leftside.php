<?php
    use app\components\Common;
?>
<div class="left-menu">
    <ul>
        <?php
        $categories = Common::getLeftCategories();
        foreach ($categories as $category):
            ?>
            <li>
                <a href="/catalog/<?= $category->id ?>"><?= $category->name; ?> </a>
                <?php if (count($category->products)): ?>

                    <ul class="dropdown">
                        <?php foreach ($category->products as $product): ?>
                            <?php if (\yii\helpers\Url::to() == '/catalog/view?id=' . $product->id): ?>
                                <li class="active">
                                    <a href="/catalog/view/<?= $product->id ?>"><?= $product->name ?></a>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a href="/catalog/view/<?= $product->id ?>"><?= $product->name ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>


                <?php endif; ?>
            </li>

        <?php endforeach; ?>
    </ul>
</div>
<!--<div class="left-news">-->
<!--    <h4>новости</h4>-->
<!--    --><?php
//    $model = Common::getLeftNews();
//    foreach ($model as $item):
//        ?>
<!--        <div class="new">-->
<!--            <div class="row">-->
<!--                <div class="col-sm-1 pr0">-->
<!--                    <img src="/images/new-icon.png">-->
<!--                </div>-->
<!--                <div class="col-sm-11">-->
<!--                    <span class="date">--><?//= Common::getFormatDate($item->created_at) ?><!--</span>-->
<!--                    <p><a href="/news/--><?//= $item->id ?><!--">--><?//= $item->name ?><!--</a></p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    --><?php //endforeach; ?>
<!--    --><?php //if (count(Common::getLeftNews()) > 4): ?>
<!--        <h4><a href="/news">все новости</a></h4>-->
<!--    --><?php //endif; ?>
<!--</div>-->