<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'action' => ['search/index'],
    //'enableClientValidation' => false,

    'method' => 'post',
    'options' => ['class' => 'form-inline'],
]) ?>

<?= $form->field($model, 'search_query')->textInput()->label(false)->input('q',['placeholder' => 'Поиск по сайту']) ?>
<?= Html::submitButton('<img src="/images/search-button.png">', ['class' => 'btn']) ?>
<?php ActiveForm::end() ?>