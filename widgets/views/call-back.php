<div class="modal fade bs-example-modal-sm-callback" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <p><strong>Вы можете узнать подробнее о продукции по телефону <a href="tel:8-347-287-81-72">8-347-287-81-72</a> иди можете:</strong>
            </p>
            <div class="h3"><img src="/images/phone-icon.png"> Заказать звонок</div>
            <?php
            use yii\widgets\ActiveForm;
            use yii\helpers\Html;

            $form = ActiveForm::begin([
                'action' => ['site/callback'],
                'enableAjaxValidation' => true,
                'options' => [
                    'class' => 'callback-form',
                    'validateOnSubmit' => true
                ],
            ]);
            ?>
            <?= $form->field($model, 'subject')->hiddenInput(['value' => 'Заказ звонка с сайта shihan-oil.com'])->label(false) ?>
            <?= $form->field($model, 'name')->label('Имя') ?>
            <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7 (999)-999-9999',
                'clientOptions' => [
                    'clearIncomplete' => true
                ]
            ])->label('Номер телефона') ?>
            <?= Html::submitButton('Заказать', ['class' => 'btn btn-primary btn-md']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<script>
//    var submit = $('.callback-form button');
//    $(submit).on('click', function () {
//        $.post('/site/callback', $('.callback-form ').serialize(), function (data) {
//            $('.close').trigger('click');
//            alert('Ваше сообщение доставлено, наши менеджеры свяжутся с вами в ближайшее время!');
//        });
//        return false;
//    })

</script>
