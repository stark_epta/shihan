<div class="modal fade bs-example-modal-sm-sendprice" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <p><strong>Отправьте нам свои контактные данные и: </strong>
            </p>
            <div class="h3"><img src="/images/phone-icon.png"> Получите прайс-лист</div>
            <?php
            use yii\widgets\ActiveForm;
            use yii\helpers\Html;

            $form = ActiveForm::begin([
                'action' => ['site/callback'],
                'enableAjaxValidation' => true,
                'options' => [
                    'class' => 'callback-form',
                    'validateOnSubmit' => true
                ],
            ]);
            ?>
            <?= $form->field($model, 'subject')->hiddenInput(['value' => 'Заказ прайс-листа с сайта shihan-oil.com'])->label(false) ?>
            <?= $form->field($model, 'name')->label('Имя') ?>
            <?= $form->field($model, 'email')->label('Ваш E-mail') ?>
            <?= $form->field($model, 'phonesend')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7 (999)-999-9999',
                'clientOptions' => [
                    'clearIncomplete' => true,
                    'aria-invalid' => true
                ]
            ])->label('Номер телефона') ?>
            <?= Html::submitButton('Заказать', ['class' => 'btn btn-primary btn-md']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<script>
//    var submit = $('.price-form button');
//    $(submit).on('click', function () {
//        $.post('/site/callback', $('.price-form ').serialize(), function (data) {
//            $('.close').trigger('click');
//            alert('Прайс-лист нашей компании отправлен на указанный Вами e-mail');
//        });
//        return false;
//    })
</script>
