<?php
namespace app\widgets;

use app\models\SearchForm;
use yii\bootstrap\Widget;


class CallBackWidget extends Widget {
    public function run() {

        $model = new \app\models\CallbackMailer();
        return $this->render('call-back',['model' => $model]);
    }
}