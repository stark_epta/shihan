<?php

namespace app\widgets;
use yii\bootstrap\Widget;


class MyNavbarWidget extends Widget {
    public function run() {
        return $this->render('mynavbar');
    }
}