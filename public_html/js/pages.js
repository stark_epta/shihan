$('#pages-dropdown_menu label:nth-child(1) input').addClass('yes');
$('#pages-dropdown_menu label:nth-child(2) input').addClass('no');

if ($('#pages-dropdown_menu input.yes').is(':checked')) {
    $('.field-pages-parent_id').show()
}
var yes = $('#pages-dropdown_menu input.yes');
var no = $('#pages-dropdown_menu input.no');

$(yes).click(function () {
    $('.field-pages-parent_id').show()
});
$(no).click(function () {
    $('.field-pages-parent_id').hide()
});
