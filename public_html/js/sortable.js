$('table#sortable tbody').sortable({
    forcePlaceholderSize: true,
    forceHelperSize: true,
    items: 'tr',
    update: function () {
        serial = $('table tbody').sortable('serialize', {key: 'items[]', attribute: 'id'});

        $.ajax({
            'url': '/admin/common/sort?table='+$('.table_name').text(),
            'type': 'post',
            'data': serial,
            'success': function(data){
            },

            'error': function(request, status, error){
                alert('We are unable to set the sort order at this time.  Please try again in a few minutes.');
            }
        });
    },
    //helper: fixHelper
}).disableSelection();