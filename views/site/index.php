<?php
$this->title = 'Продажа нефтепродуктов и нефтехимии | Шиханы Ойл ';
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => 'Продажа нефтехимической продукции в Уфе и по Республике. 8-347-287-81-72'
]);
?>
<div class="row">
    <div class="slider col-sm-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!--<ol class="carousel-indicators">-->
            <!--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>-->
            <!--<li data-target="#carousel-example-generic" data-slide-to="1"></li>-->
            <!--<li data-target="#carousel-example-generic" data-slide-to="2"></li>-->
            <!--</ol>-->

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active ">
                    <div class="h1">Качественная продукция<br> своевременная логистика<br> индивидуальный подход</div>
                    <a =href="" data-toggle="modal" data-target=".bs-example-modal-sm-callback" class="btn btn-primary callback">Подробнее</a>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
    <h3 class="col-sm-12">КАТАЛОГ ПРОДУКЦИИ</h3>
</div>
<div class="row catalog-main">
    <?php
    $i = 0;
    foreach ($category as $item):
        ?>
        <div class="col-sm-4 text-center">
            <div class="catalog-item">
                <img src="/uploads/category/<?= $item->image ?>">
                <h4><a href="/catalog?id=<?= $item->id; ?>"><?= $item->name; ?></a></h4>
                <ul class="list-unstyled">
                    <?php
                    $j = 0;
                    foreach ($item->products as $product):
                        $j++;
                        ?>
                        <li><a href="/catalog/view?id=<?= $product->id ?>"><?= $product->name; ?></a></li>
                        <?php
                            if ($j == 5) {
                                break;
                            }
                        ?>
                    <?php endforeach; ?>
                    <?php if (count($item->products) > 5): ?>
                        <li><a class="more" href="/catalog?id=<?= $item->id ?>">Еще</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php
        $i++;
        if ($i % 3 == 0):
            ?>
            <div class="clearfix"></div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>