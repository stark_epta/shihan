<?php
use himiklab\colorbox\Colorbox;
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/catalog']];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['/catalog/'.$model->id]];
$this->params['breadcrumbs'][] = $model->name;
$this->title = $model->meta_title;
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_description
]);

?>
<?= Colorbox::widget([
    'targets' => [
        '.colorbox' => [
            'maxWidth' => 1000,
            'maxHeight' => 750,
        ],
    ],
    'coreStyle' => 2
]) ?>
<div class="view">
    <h1><?= $model->name; ?></h1>
    <div class="text">
        <?= $model->description; ?>
    </div>
    <?php if(!empty($images)): ?>
    <div class="passports">
        <h2>Паспорта продукции: <strong><?= $model->name; ?></strong></h2>
        <?php foreach ($images as $image): ?>
            <div class="col-sm-4 text-center">
                <a rel="gal" href="/uploads/products/<?= $model->id ?>/<?= trim($image,'small_'); ?>" class="colorbox" data-toggle="lightbox"><img src="/uploads/products/<?= $model->id ?>/<?= $image; ?>"></a>
            </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>