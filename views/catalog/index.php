<?php
$this->params['breadcrumbs'][] = 'Каталог';
$this->title = 'Каталог нефтепродуктов в Уфе | Шиханы Ойл ';
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => 'Доставка нефтепродуктов и нефтехимии по Уфе и Республике в . 8-347-287-81-72'
]);
?>
<div class="row catalog-main">
    <?php
    $k = 0;
    foreach ($category as $item):
        ?>
        <div class="col-sm-4 text-center">
            <div class="catalog-item">
                <img src="/uploads/category/<?= $item->image; ?>">
                <h4><a href="/catalog/<?= $item->id; ?>"><?= $item->name; ?></a></h4>
                <ul class="list-unstyled">
                    <?php
                    $i = 0;
                    foreach ($item->products as $product):
                    $i++;
                        ?>
                        <li><a href="/catalog/view/<?= $product->id ?>"><?= $product->name; ?></a></li>
                        <?php
                            if ($i == 5) {
                                break;
                            }
                        ?>
                    <?php endforeach; ?>
                    <?php if (count($item->products) > 5): ?>
                        <li><a class="more" href="/catalog/<?= $item->id ?>">Еще</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php
        $k++;
        if ($k % 3 == 0):
            ?>
            <div class="clearfix"></div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

