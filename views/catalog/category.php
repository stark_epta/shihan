<?php


$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/catalog']];
$this->params['breadcrumbs'][] = $category->name;
$this->title = $category->meta_title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $category->meta_description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $category->meta_keywords
]);

?>
<div class="category row">
    <h1 class="col-sm-12"><img src="/uploads/category/<?= $category->image; ?>"><?= $category->name ?></h1>
    <div class="col-sm-12 description"><?= $category->description ?></div>
    <ul class="col-sm-12">
        <?php foreach ($products as $product): ?>
            <li><a href="/catalog/view/<?= $product->id ?>"><?= $product->name ?></a> </li>
        <?php endforeach; ?>

    </ul>
    <div class="col-sm-12 another-category">
        <?php foreach ($another_category as $item): ?>
            <h3><img src="/uploads/category/<?= $item->image; ?>"><a href="/catalog/<?= $item->id ?>"><?= $item->name ?></a></h3>
        <?php endforeach; ?>

    </div>
</div>