<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\DefaultAsset;
use app\components\Common;
use yii\widgets\MaskedInput;


DefaultAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title); ?></title>
        <?php $this->head() ?>

    </head>
    <body>
    <?php $this->beginBody() ?>

    <?php if (Yii::$app->session->hasFlash('success-send-email')): ?>
        <?php
        $success = Yii::$app->session->getFlash('success-send-email');
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-info'
            ],
            'body' => $success
        ])
        ?>
        <?php
    endif;
    ?>
    <div class="container-fluid header">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <a href="/"><img src="/images/logo.png"></a>
                </div>
                <div class="col-sm-3">
                    <p>Нефтехимическая продукция<br>и топливные присадки</p>
                </div>
                <div class="col-sm-2 header-phone">
                    <div class="row">
                        <div class="col-sm-3 pl0">
                            <img src="/images/phone-icon.png">
                        </div>
                        <div class="col-sm-9 pr0">
                            <a href="tel:+7(347)287-81-72">8-347-287-81-72</a><br><a class="callback" href=""
                                                                                     data-toggle="modal"
                                                                                     data-target=".bs-example-modal-sm-callback">Заказать
                                звонок</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 search-block">
                    <? echo app\widgets\SearchWidget::widget() ?>
                </div>
            </div>
        </div>
        <div class="container">
            <?= \app\widgets\MyNavbarWidget::widget(); ?>
        </div>
    </div>
    <div class="container main-section">
        <div class="row">
            <div class="col-sm-3 left">
                <? echo app\widgets\LeftSideWidget::widget() ?>
            </div>
            <div class="col-sm-9 right">
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]); ?>
                <?= $content ?>

            </div>
        </div>
    </div>
    <div class="container-fluid footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <img src="/images/logo.png">
                    <p><strong>456049, Республика Башкортостан
                            г. Уфа, ул Майкопская 56, офис 7</strong></p>
                    <p><strong>Телефон:</strong> +7(347)287-81-72</p>
                    <p><strong>E-mail: </strong><a href="mailto:shihanoil@yandex.ru">shihanoil@yandex.ru</a></p>
                    <p class="copyrite">© 2017 Компания «Шиханы-Ойл»</p>
                </div>
                <div class="col-sm-6">
                    <?php if (!Yii::$app->request->get('url') == 'contact'): ?>
                        <script type="text/javascript" charset="utf-8" async
                                src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=h2ooScgW3Z1lVQurm8XhZtMun2XYUIVg&amp;width=100%&amp;height=250&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
                    <?php endif; ?>
                </div>
                <div class="col-sm-3">
                    <ul class="list-unstyled">
                        <?php
                        $model_menu = \app\models\Pages::find()
                            ->where(['status' => 1])
                            ->andWhere(['menu' => 1])
                            ->andWhere(['dropdown_menu' => 0])
                            ->all();
                        foreach ($model_menu as $item):
                            ?>
                            <li><a href="/<?= $item->url ?>"><?= $item->name ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <a class="getPrice" href=""
       data-toggle="modal"
       data-target=".bs-example-modal-sm-sendprice"><span class="glyphicon glyphicon-usd"></span> Получить наш
        прайст-литс</a>
    <? echo app\widgets\CallBackWidget::widget() ?>
    <? echo app\widgets\SendPrice::widget() ?>
    <!— Yandex.Metrika counter —>
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter43916079 = new Ya.Metrika({
                        id: 43916079,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/43916079" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!— /Yandex.Metrika counter —>
    <div style="display:none" itemscope itemtype="http://schema.org/LocalBusiness"><h3><span
                    itemprop="name">Шиханы Ойл</span></h3><span itemprop="description">Оптовая и розничная продажа нефтепродуктов и нефтехимии!</span><br/><span
                itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">г. Уфа, ул Майкопская 56, офис 7</span><br/><span
                    itemprop="postalCode">450095</span><br/><span
                    itemprop="addressCountry">Россия</span><br/>Phone: <span
                    itemprop="telephone">+7(347)287-81-72</span><br/></div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>