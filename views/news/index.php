<?php
use app\components\Common;
use yii\widgets\LinkPager;

$this->params['breadcrumbs'][] = 'Статьи';
$this->title = 'Статьи | Шиханы Ойл';
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => 'Статьи'
]);
?>
<div class="news">
    <h1>Статьи</h1>
    <?php foreach ($model as $item): ?>
        <div class="new">
            <h2><a href="/news/view?id=<?= $item->id ?>"><?= $item->name ?></a></h2>
            <span class="date"><?= Common::getFormatDate($item->created_at); ?></span>
        </div>
    <?php endforeach; ?>
</div>
<?php echo LinkPager::widget([
    'pagination' => $pages,
]); ?>