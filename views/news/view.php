<?php
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => '/news'];
$this->params['breadcrumbs'][] = $model->name;
$this->title = $model->meta_title;
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_description
]);
?>
<div class="news-view">
    <h1><?= $model->name; ?></h1>
    <div class="text">
        <?= $model->description; ?>
    </div>
</div>