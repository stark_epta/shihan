<?php
    if (!$search_query) {
        $this->title = 'Страница поиска | Шиханы Ойл ';
    } else {
        $this->title = 'Результаты поиска - '.$search_query.' | Шиханы Ойл ';
    }
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => 'Продажа нефтехимической продукции в Уфе и по Республике. 8-347-287-81-72'
]);
?>
<div class="search-result">
    <?php if (!$search_query): ?>
        Страница поиска
    <?php endif; ?>
    <?php if ($search_query): ?>
        <?php if ((!count($model_product)) && (!count($model_news))): ?>
            <h2>По запросу: <u><?= $search_query ?></u> ничего не найдено.</h2>
        <?php endif; ?>
        <?php if (count($model_product)): ?>
            <h3>Найдено продукции <?= count($model_product) ?>:</h3>
            <?php foreach ($model_product as $item): ?>
                <p><strong><a href="/catalog/view?id=<?= $item->id ?>"><?= $item->name ?></a></strong></p>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if (count($model_news)): ?>
            <h3>Найдено новостей <?= count($model_news) ?>:</h3>
            <?php foreach ($model_news as $item): ?>
                <p><strong><a href="/news?id=<?= $item->id ?>"><?= $item->name ?></a></strong></p>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endif; ?>
</div>
