<?php
namespace app\controllers;

use app\models\Pages;
use yii\web\Controller;

class PagesController extends Controller
{
    public $layout = 'bootstrap';
    public function actionIndex($url)
    {
        if (!$model = Pages::findOne(['url' => $url])) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена');
        }
        return $this->render('index', ['model' => $model]);
    }
}