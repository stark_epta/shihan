<?php
    namespace app\controllers;

    use app\components\Common;
    use Yii;
    use yii\data\ArrayDataProvider;
    use yii\data\Pagination;
    use app\models\News;
    use yii\helpers\FileHelper;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;

    class NewsController extends Controller {

        public $layout = 'bootstrap';

        public function actionIndex() {


            $model = News::find()->orderBy('created_at');
            $countQuery = clone $model;
            $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize' => 15]);
            $model = $model->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            return $this->render('index',[
                'model' => $model,
                'pages' => $pages
            ]);
        }
        public function actionView($id) {
            $model = $this->findModel($id);
            return $this->render('view',['model' => $model]);
        }
        public function findModel($id) {
            if (($model = News::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('Этой страницы не существует');
            }
        }

    }