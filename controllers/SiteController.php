<?php

namespace app\controllers;

use app\components\Common;
use app\models\CallbackMailer;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm as Login;
use app\models\Category;
use app\models\ContactForm;
use app\models\SignUp;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public $layout = 'bootstrap';
    public function actionIndex()
    {

        $category = Category::find()->orderBy('ordering')->all();
        return $this->render('index', [
            'category' => $category,
        ]);

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionSignup()
    {
        $model = new Signup();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($user = $model->signup()) {
                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionCallback() {
        $model = new CallbackMailer();
        $email = false;
        $phone = false;
        $phonesend = false;
        if(\Yii::$app->request->isAjax && \Yii::$app->request->isPost){
            if($model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $name = $model->name;
            $subject = $model->subject;
            if (isset($phone)) {
                $phone = $model->phone;
            }
            if (isset($phonesend)) {
                $phonesend = $model->phonesend;
            }
            if (isset($model->email)) {
                $email = $model->email;
                //$model->sendPrice('Прайс-лист компании Shihan-oil',$email);
            }
            if ($model->sendEmail($name,$phone,$subject,$email,$phonesend)) {
                if ($model->email) {
                    \Yii::$app->session->setFlash('success-send-email', 'Прайс-лист был выслан на указанную Вами почту, наши менеджеры свяжутся с Вами в ближайшее время');
                } else {
                    \Yii::$app->session->setFlash('success-send-email', 'Ваше письмо успешно отправлено, наши менеджеры свяжутся с Вами в ближайшее время');
                }
                $this->redirect('/');
            }
            return true;
        }
    }



    public function action404() {
        echo '404';
    }
}
