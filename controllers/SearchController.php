<?php

namespace app\controllers;

use app\models\News;
use app\models\Product;
use app\models\SearchForm;
use yii\web\Controller;

class SearchController extends Controller
{
    public $layout = 'bootstrap';

    public function actionIndex()
    {
        $search_query = false;
        $model = new SearchForm();
        if ($model->load(\Yii::$app->request->post())){
            $search_query = $model->search_query;
            $model_news = News::find()->filterWhere([
                'or',
                ['like', 'name', '%' . $search_query . '%', false],
                ['like', 'description', '%' . $search_query . '%', false]
            ])->all();
            $model_product = Product::find()->filterWhere([
                'or',
                ['like', 'name', '%' . $search_query . '%', false],
                ['like', 'description', '%' . $search_query . '%', false]
            ])->all();
        }

        return $this->render('index', [
            'search_query' => $search_query,
            'model_news' => $model_news,
            'model_product' => $model_product,

        ]);
    }
}