<?php
namespace app\controllers;

use app\models\Category;
use app\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\FileHelper;


class CatalogController extends Controller
{
    public $layout = 'bootstrap';

    public function actionIndex($id = false)
    {
        if (!$id) {
            $category = Category::find()->orderBy('ordering')->all();
            return $this->render('index', [
                'category' => $category,
            ]);
        } else {
            $category = new Category();
            $category = $this->findModel($id);
            $products = $category->products;
            $another_category = Category::find()->where('id != ' . $id)->orderBy('ordering')->all();
            return $this->render('category', [
                'category' => $category,
                'products' => $products,
                'another_category' => $another_category
            ]);
        }

    }

    public function actionView($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            $path = \Yii::getAlias("@app/public_html/uploads/products/".$id);
            $files = FileHelper::findFiles($path);
            $images = [];
            foreach ($files as $file) {
                if (stristr($file,'small_')) {
                    $images[] = basename($file);
                }
            }
            $model = Product::findOne($id);
            return $this->render('view', [
                'model' => $model,
                'images' => $images,
            ]);
        } else {
            throw new NotFoundHttpException('Этой страницы не существует');
        }
    }

    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Этой страницы не существует');
        }
    }
}